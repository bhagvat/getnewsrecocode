//initiallize redis userProfile
var redis = require('./redis');
redis.select(1);
var UserProfile = require('./user_profile');
var mongoquery = require('./mongo_query');


//set up arrangoDB
var Database = require('arangojs').Database;
var aql = require('arangojs').aql;
var arango_db = new Database('http://ec2-34-215-65-5.us-west-2.compute.amazonaws.com:8529')
arango_db.useBasicAuth('root', 'i-0172f1f969c7548c4');
arango_db.useDatabase('newsDB');
var graph = arango_db.graph('newsGraph');


//input Json 
var profile = {
    "userUUID": "df7cca36-3d7a-40f4-8f06-ae03cc22f045",
    "articleBeingRead": {
        "docID": "624",
        "image": "http://akamai.vidz.zeecdn.com/ozee/shows/total-dreamer/listing-image-small-500.jpg?v=1497885701",
        "title": "total dremers",
        //"url": "http://www.ozee.com/shows/total-dreamer?reco=true",

        "url": "http://www.wionews.com/south-asia/in-bid-to-improve-relations-us-senator-reassures-pakistan-of-support-in-terror-fight - 1739"
    },
    "site": "Zee News"
};


var history = [];

var userID = profile.userUUID;
var articleID = profile.articleBeingRead.docID;
var articleUrl = profile.articleBeingRead.url;
var newuser = {};
var returnMsg;
var recolist = [];
var userData;
var articleData;
var title = profile.articleBeingRead.title;
var history = profile.articleBeingRead;


var newUserCache = {};


//all vars used
var cacheArticle;








//  this following checks for the artcle is present if there sent statics reco msg for now
//  if the article is not there get the article from db


UserProfile.get(profile.articleBeingRead.url, function(err, articleData) {
    if (err) {
        console.log(err);
        throw err;
    } else {
        console.log(articleData);
        console.log("inside cashe of article");
        articleData = articleData;

        //check if article not found on cache
        if (articleData) {
            //countinue code
            //add history to cache
            console.log("recommendations found in cache");

            //if artcile is not found in cache , go to Arr_DB and found   
        } else {
            if (articleData == null || undefined) {
                // need to get article from DB
                // assign Article newArticleID = result.ArticleID
                arango_db.query("FOR doc IN Document FILTER doc.docID==@articleid RETURN doc", { 'articleid': articleID }, function(err, cursor) {
                    if (err) {
                        console.error(err);
                    } else {

                        debugger
                        cursor.next(function(err, result) {
                            //check if doc is not present in ARR_DB
                            console.log(result);
                            if (result == null) {

                                console.log("1.get related docs based on title");
                                //arrangoDB Qiery for get related docs based on title
                                //pass profile.articleBeingRead.url 
                                arango_db.query("FOR doc IN Document FILTER doc.title==@articletitle RETURN doc", { 'articletitle': title }, function(err, cursor) {
                                    if (err) {
                                        console.error(err);
                                    } else {
                                        if (result == null) {
                                            //go for crawling 
                                        } else {
                                            //store result on cache
                                            cursor.next(function(err, result) {
                                                var articledata = result;
                                                UserProfile.set(articledata.articleID, articledata, function(err) {
                                                    if (err) {
                                                        throw err;
                                                    }
                                                    console.log("article  is saved to cache");
                                                    UserProfile.setexpire(articledata.articleID);
                                                });
                                            });

                                        }
                                    }
                                });

                            } else {
                                //store on cache
                                //articledata contains the whole article document (`articleID`,`url`,`title` and other attributes)
                                var articledata = result;
                                UserProfile.set(articleUrl, articledata, function(err) {
                                    if (err) {
                                        throw err;
                                    }
                                    console.log("article  is saved to cache");
                                    UserProfile.setexpire(articledata.articleID);
                                });


                            }
                        });
                    }
                });

                //if close gere
            }

        }
    }
});

//-----------------------------------------------below is user function---------------------------------------------//
//following code does the 
// - check user is available in cashe or not 
// - if avilable update user history with the current article details
// - user is not there create session var (unique id for user) and store user with his hstory
// - after ttl over write this user to arr_DB



UserProfile.get(userID, function(err, userprofile) {
    if (err) {
        //if err throw err
        console.log(err);
        throw err;
    } else {
        if (userprofile) {
            //if user found on cache 
            //update user history
            // profile.history
            console.log("user found on cache" + userprofile);
            console.log("history updated + return user with reco");

            var sessionID = new Date();
            console.log("session ID" + sessionID);
            profile.history.push = {
                "session_ID": sessionID,
                "url1": "some dummy url",
                "time": "time of view article",
                "ref": "some referaence for article"
            }


            console.log("user history updated");


        }
        //user not found on cache 
        else if (userprofile == null || undefined) {
            // if user is not found in cache 
            // go for arr_DB and found user
            arango_db.query("FOR user IN Users FILTER user.userID==@userid RETURN user", { 'userid': userUUID }, function(err, cursor) {
                if (err) {
                    console.error(err);
                } else {
                    cursor.next(function(err, result) {
                        if (result == null) {
                            // if new user is not found on ARR_DB
                            // insert new user to redis
                            var date_added = new Date();
                            var newuser = {
                                "UUID": userID,
                                "location": "pune",
                                "OS": "windows",
                                "browser": "chrome",
                                "date_added": date_added,
                                "last_updated": date_added,
                                "history": null
                            }

                            UserProfile.set(userID, newuser, function(err) {
                                if (err) {
                                    throw err;
                                }
                                console.log("user is saved to cache");
                                UserProfile.setexpire(userID);
                            });
                        } else {


                        }
                    });
                }
            });

        }
    }
});


//---------------final function----------------------------------------//
UserProfile.get(userID, function(err, userprofile) {
    //check user is in cache or not
    //main case 1.
    if (err) {
        //if err throw err
        console.log(err);
        throw err;

    } else if (userprofile) {
        //main case 2
        //if user is found on cache go for checking Article on cache
        UserProfile.get(articleUrl, function(err, cacheArticle) {
            if (err) {
                console.log(err);
                throw err;
            } else {
                if (cacheArticle == null || undefined) {
                    // if article not found on cache
                    //find article in DB
                    arango_db.query("FOR doc IN Document FILTER doc.docID==@articleid RETURN doc", { 'articleid': articleID }, function(err, cursor) {
                        if (err) {
                            console.error(err);
                        } else {
                            cursor.next(function(err, result) {
                                //if article is not found on ARR_DB
                                if (result == null || undefined) {
                                    //call kavinsFun(null,null, userprofile)
                                    //push related articles to redis

                                    /* following code is for save to redis
                                    var recolist = "somekey"
                                      UserProfile.set(recolist, articledata, function(err) {
                                          if (err) {
                                              throw err;
                                          }
                                          console.log("reco article saved to cache");
                                          UserProfile.setexpire(recolist);
                                      }); */

                                    console.log("case:user found , article not found on DB: you get the followig recommendations");
                                }
                                //if article found on ARR_DB
                                else {

                                    //call to kavinsFun(DBarticle, ClusterID , userprofile)
                                    //push recommended articles to redis

                                    /* following code is for save to redis
                                      UserProfile.set(recolist, articledata, function(err) {
                                          if (err) {
                                              throw err;
                                          }
                                          console.log("reco article  is saved to cache");
                                          UserProfile.setexpire(recolist);
                                      }); */

                                    console.log("case:user found,article found on DB: recomendation saved to redis, you get the following recomendation");
                                }
                            });
                        }
                    });
                } else {
                    //check here user found as well as article is found
                    //return article history
                    console.log("case:if user found ,article found : recommendation are found in redis");
                }
            }
        });
    }
    //maincase.3
    else if (userprofile == null || undefined) {
        //if user profile is not in redis go for arr_DB
        arango_db.query("FOR user IN Users FILTER user.userID==@userid RETURN user", { 'userid': userUUID }, function(err, cursor) {
            if (err) {
                console.error(err);
            } else {
                cursor.next(function(err, result) {
                    if (result == null || undefined) {
                        // if user not found on cache make entry on cache
                        UserProfile.set(userID, profile, function(err) {
                            if (err) {
                                throw err;
                            }
                            console.log("reco article saved to cache");
                            UserProfile.setexpire(userID);
                        });

                        //now check for articles
                        UserProfile.get(articleUrl, function(err, cacheArticle) {
                            if (err) {
                                console.log(err);
                                throw err;
                            } else {
                                // if article not found on cache
                                // find article in DB
                                if (cacheArticle == null || undefined) {
                                    arango_db.query("FOR doc IN Document FILTER doc.docID==@articleid RETURN doc", { 'articleid': articleID }, function(err, cursor) {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            cursor.next(function(err, result) {
                                                if (result == null || undefined) {
                                                    // if article not found on db
                                                    //get related articles and save to redis
                                                    //call here KavinFun(null, ClusterID , UUID.obj)

                                                    /* following code is for save to redis
                                                    var recolist = "somekey"
                                                    UserProfile.set(recolist, articledata, function(err) {
                                                    if (err) {
                                                       throw err;
                                                     }
                                                      console.log("reco article saved to cache");
                                                      console.log("case:user not found (make entry to db , meanse now found)and article not found , you have following recomendation")
                                                      UserProfile.setexpire(recolist);
                                                     }); */
                                                    console.log("case:user not found (make entry to db , meanse now found)and article not found , you have following recomendation")
                                                } else {
                                                    // if article  found on ARR_DB , make reco_list entry to redis
                                                    // call KavinsFun((Article_obj, ClusterID , UUID.obj)
                                                    /* following code is for save to redis
                                                    var recolist = "somekey"
                                                    UserProfile.set(recolist, articledata, function(err) {
                                                    if (err) {
                                                       throw err;
                                                     }
                                                      console.log("reco article saved to cache");
                                                      console.log("case:user not found (make entry to db , meanse now found)and article not found , you have following recomendation")
                                                      UserProfile.setexpire(recolist);
                                                     }); */

                                                    console.log("case: user not found (make entry in db, ,eanse now found)and article found");
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    //if documet found on cache +user we have made addded now
                                    console.log("article found on cache,return recolist by comparing user history");
                                    //call KavinsFun(Article_obj, ClusterID , UUID.obj)
                                    console.log("case: user not found(make entry in DB meanse now available), article Found on cache : you have following reco list found");
                                }
                            }
                        });
                    } else {
                        //if user found on ARR_DB
                        //check for Article

                        UserProfile.get(articleUrl, function(err, cacheArticle) {
                            if (err) {
                                console.log(err);
                                throw err;
                            } else {
                                if (cacheArticle == null || undefined) {
                                    //check here if article is not found on cache
                                    //find article in DB
                                    arango_db.query("FOR doc IN Document FILTER doc.docID==@articleid RETURN doc", { 'articleid': articleID }, function(err, cursor) {
                                        if (err) {
                                            console.error(err);
                                        } else {

                                            cursor.next(function(err, result) {
                                                //if article not found on ARR_DB
                                                if (result == null || undefined) {
                                                    //call kavinsFun(null,clusterID, userprofile)
                                                    //push related articles to redis

                                                    /* following code is for save to redis
                                                    var recolist = "somekey"
                                                      UserProfile.set(recolist, articledata, function(err) {
                                                          if (err) {
                                                              throw err;
                                                          }
                                                          console.log("reco article saved to cache");
                                                          UserProfile.setexpire(recolist);
                                                      }); */

                                                    console.log("case:user found on redis, article not found on DB: you get the followig recommendations");


                                                } else {
                                                    // if article  found on ARR_DB , make reco_list entry to redis
                                                    // call KavinsFun((Article_obj, ClusterID , UUID.obj)
                                                    /* following code is for save to redis
                                                    var recolist = "somekey"
                                                    UserProfile.set(recolist, articledata, function(err) {
                                                    if (err) {
                                                       throw err;
                                                     }
                                                      console.log("reco article saved to cache");
                                                      console.log("case:user not found (make entry to db , meanse now found)and article not found , you have following recomendation")
                                                      UserProfile.setexpire(recolist);
                                                     }); */

                                                    console.log("case:user found on redis article found on ARR_DB: you have following recomendation")
                                                }
                                            });
                                        }
                                    })
                                } else {
                                    //check here if article is found on cache
                                    //return result
                                    //call KavinsFun(Article_obj, ClusterID , UUID.obj)
                                    console.log("case:user found on redis article found on redis: you have following recomendation");
                                }
                            }
                        });
                    }
                });
            }
        });
    }

});





//check if user not found on cashe
// if (userData) {
//     //countinue code
//     //add history to cache
//     console.log("recommendations found in cashe");
//     //if artcile is not found in cache , go to Arr_DB and found   
// } else {

//     // need to get user from DB
//     // assign user newUserID = result.userID

//     arango_db.query("FOR user IN Users FILTER user.userID==@userid RETURN user", { 'userid': userID }, function(err, cursor) {
//         if (err) {
//             console.error(err);
//         } else {
//             cursor.next(function(err, result) {
//                 if (err) {
//                     console.log(err);
//                 } else {

//                     if (result == null) {
//                         //save user to arrangoDB
//                     } else {
//                         //on new user first save user to redis and save to arrangoDB
//                         // user along with his history
//                         newuser.UserID = userID;
//                         newuser.History = history;
//                         UserProfile.set(newuser.UserID, newuser, function(err) {
//                             if (err) {
//                                 throw err;
//                             }
//                             console.log("new user is saved to cache");
//                             // set ttl here , after ttl over save user to ARR_DB
//                             // UserProfile.setexpire(articledata.articleID);
//                         });


//                     }
//                 }
//             });
//         }
//     });
// }
















// else(userData.userprofile == null) {
//     //need to get user from DB
//     //assign USerUUID newUserID = result.Userid
// }

// //if articleID is not Found in DB
// if (newArticleID == null) {
//     //get the possible reco based on the Article title , i.e profile.articleBeingRead.title
//     //get Recolist
//     //else crawl here 
//     //get reco list
// } else {
//     //kavin sirs code

// }

// //user does not exist code



//--------------------------------------------------------my needed code --------------------------//

//check if article not found on cache
// if (articleData) {
//     //countinue code
//     //add history to cache
//     console.log("recommendations found in cache");

//     //if artcile is not found in cache , go to Arr_DB and found   
// } else {
//     if (articleData == null || undefined) {
//         // need to get article from DB
//         // assign Article newArticleID = result.ArticleID
//         arango_db.query("FOR doc IN Document FILTER doc.docID==@articleid RETURN doc", { 'articleid': articleID }, function(err, cursor) {
//             if (err) {
//                 console.error(err);
//             } else {

//                 debugger
//                 cursor.next(function(err, result) {
//                     //check if doc is not present in ARR_DB
//                     console.log(result);
//                     if (result == null) {

//                         console.log("1.get related docs based on title");
//                         //arrangoDB Qiery for get related docs based on title
//                         //pass profile.articleBeingRead.url 
//                         arango_db.query("FOR doc IN Document FILTER doc.title==@articletitle RETURN doc", { 'articletitle': title }, function(err, cursor) {
//                             if (err) {
//                                 console.error(err);
//                             } else {
//                                 if (result == null) {
//                                     //go for crawling 
//                                 } else {
//                                     //store result on cache
//                                     cursor.next(function(err, result) {
//                                         var articledata = result;
//                                         UserProfile.set(articledata.articleID, articledata, function(err) {
//                                             if (err) {
//                                                 throw err;
//                                             }
//                                             console.log("article  is saved to cache");
//                                             UserProfile.setexpire(articledata.articleID);
//                                         });
//                                     });

//                                 }
//                             }
//                         });

//                     } else {
//                         //store on cache
//                         //articledata contains the whole article document (`articleID`,`url`,`title` and other attributes)
//                         var articledata = result;
//                         UserProfile.set(articleUrl, articledata, function(err) {
//                             if (err) {
//                                 throw err;
//                             }
//                             console.log("article  is saved to cache");
//                             UserProfile.setexpire(articledata.articleID);
//                         });


//                     }
//                 });
//             }
//         });

//         //if close gere
//     }

// ,l[]}




-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

{
    site: 'Zee News',
    userUUID: 'df7cca36-3d7a-40f4-8f06-ae03cc224555',
    History: '[ {
    "article_Mon Dec 11 2017 14:56:20 GMT+0530 (IST)": {
        "docID": "624",
        "image": "http://akamai.vidz.zeecdn.com/ozee/shows/total-dreamer/listing-image-small-500.jpg?v=1497885701",
        "title": "total dremers",
        "url": "http://www.wionews.com/south-asia/in-bid-to-improve-relations-us-senator-reassures-pakistan-of-support-in-terror-fight - 1739",
        "articleReadDateTime": "2017-12-11T09:26:20.066Z"
    }
},

{
    "article_Mon Dec 11 2017 14:56:25 GMT+0530 (IST)": {
        "docID": "624",
        "image": "http://akamai.vidz.zeecdn.com/ozee/shows/total-dreamer/listing-image-small-500.jpg?v=1497885701",
        "title": "total dremers",
        "url": "http://www.wionews.com/south-asia/in-bid-to-improve-relations-us-senator-reassures-pakistan-of-support-in-terror-fight - 1739",
        "articleReadDateTime ": "2017 - 12 - 11 T09: 26: 25.600 Z "
    }
}
]
'}



docker run--name redisnode - d redis



{
    userUUID: 'df7cca36-3d7a-40f4-8f06e03cc224',
    'Zee News': '[  {
    "history": [{
        "session-Mon Dec 11 2017 16:31:40 GMT+0530 (India Standard Time)": {
            "docID": "1047",
            "url": "http://www.wionews.com/sports/viva-shaq-obama-names-basketball-legend-as-sports-envoy-to-cuba-1136",
            "title": "Viva Shaq! Obama names basketball legend as sports envoy to Cuba."
        }
    }]
}
]
' 



var samplejson = [];
samplejson.push(profile.articleBeingRead);
var newuser = {};
var session = "session-" + new Date();
newuser.userUUID = userID;
var newuser1 = {};
var history = [];
history.push({
    [session]: samplejson
})
newuser1[site] = [{ history: history }];

newuser[site] = JSON.stringify(newuser1[site]);
console.log(newuser)
}


-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --


queryObject = {
    user: {
        UUID,
        history: [{ url1, time, ref }, { url2, time, ref }, ..],
        --
        if user does not have history then it should be an empty list--
    },
    refferednews: {
        url,
        title
    }
}

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

smaple query object

{
    'refferednews': {
        'title': 'England to go ahead with Bangladesh tour',
        'url': 'http://www.wionews.com/cricket/england-to-go-ahead-with-bangladesh-tour-5563'
    },
    'user': {
        'history': [{
            'title': 'India beat England in 4th test, take 3-0 lead in 5-match series',
            'url': 'http://www.wionews.com/cricket/india-beat-england-in-4th-test-take-3-0-lead-in-5-match-series-10036'
        }, { 'title': "India vs England Test: It's more about survival than anything for Cook and Co", 'url': 'http://www.wionews.com/cricket/india-vs-england-test-its-more-about-survival-than-anything-for-cook-and-co-8404' }]
    }
}
}, { 'title': "India vs England Test: It's more about survival than anything for Cook and Co", 'url': 'http://www.wionews.com/cricket/india-vs-england-test-its-more-about-survival-than-anything-for-cook-and-co-8404' }]
}
}