 //initiallize redis userProfile
 var redis = require('./redis');
 // redis.select(1);
 var redisRepo = require('./user_profile');
 var mongoquery = require('./mongo_query');

 //set up arrangoDB
 var Database = require('arangojs').Database;
 var aql = require('arangojs').aql;
 var arango_db = new Database('http://34.216.143.247:8529')
 arango_db.useBasicAuth('root', 'i-0172f1f969c7548c4');
 arango_db.useDatabase('newsDB');
 var graph = arango_db.graph('newsGraph');


 //var profile = event;

 //input Json 
 var profile = {
     "userUUID": "3f7cca36-3d7a-40f4-7f06-aep3ccg54fpk",
     "articleBeingRead": {
         "image": "http://akamai.vidz.zeecdn.com/ozee/shows/total-dreamer/listing-image-small-500.jpg?v=1497885701",
         "title": "total dremers",
         "url": "http://www.wionews.com/business-economy/opec-nations-to-finally-cut-down-oil-production-7958"
     },
     "site": "Zee News",
     "refferal": "yes"
 };


 var history = [];

 var userID = profile.userUUID;
 var articleID = profile.articleBeingRead.docID;
 var articleUrl = profile.articleBeingRead.url;

 var returnMsg;
 var recolist = [];
 var userData;
 var articleData;
 var title = profile.articleBeingRead.title;
 var firstArticle = profile.articleBeingRead;
 var site = profile.site;


 var newUserCache = {};
 var cacheArticle;



 redisRepo.get(userID, function(err, userprofile) {
     //check user is in cache or not
     if (err) {
         //if err throw err
         console.log(err);
         throw err;

         /*main case : if user found in cache then go for articles*/

     } else if (userprofile) {
         debugger
         //when user found update his history with current article being read

         var UserfrmChache = JSON.parse(userprofile[site]);

         var newuser = userprofile;
         var time = new Date();
         newuser.userUUID = userID;
         var newuser1 = {};
         var history = UserfrmChache[0].history;

         history.push({
             url: profile.articleBeingRead.url,
             time: time,
             ref: profile.ref
         })
         newuser1[site] = [{ history: history }];

         newuser[site] = JSON.stringify(newuser1[site]);
         console.log(newuser);
         redisRepo.set(userprofile.userUUID, newuser, function(err) {
             if (err) {
                 throw err;
             }
             console.log("reco article  is saved to cache");
             redisRepo.setexpire(userprofile.userUUID);
         });

         //if user is found on cache go for checking Article on cache
         redisRepo.get(articleUrl, function(err, cacheArticle) {
             if (err) {
                 console.log(err);
                 throw err;
             } else {
                 if (cacheArticle == null || undefined) {
                     // if article not found on cache
                     //find article in DB
                     arango_db.query("FOR doc IN Document FILTER doc.url==@url RETURN doc", { 'url': articleUrl }, function(err, cursor) {
                         if (err) {
                             console.error(err);
                         } else {
                             cursor.next(function(err, result) {
                                 debugger
                                 if (result == null || undefined) {
                                     //Complete
                                     //user found on cache (update user with history), and article is not found on db
                                     //call kavins function(userObj=userfromredis,articleObj= null,3.referral==yes,4.label='',5.cached=false )
                                     debugger
                                     var recoInput = {};
                                     recoInput.articleObj = '';
                                     recoInput.userObj = newuser;
                                     recoInput.refferal = profile.refferal;
                                     recoInput.label = "RD";
                                     console.log(recoInput);
                                     console.log("case:user found on redis , article not found on DB: you get the followig recommendations");
                                 } else {
                                     debugger
                                     //Complete
                                     //user found on cache , article is found on db , push article to redis here
                                     //call to kavinsFun(userObj=userfromredis,articleObj= null,3.referral==yes,4.label='',5.cached=false)
                                     redisRepo.set(result.url, newuser, function(err) {
                                         if (err) {
                                             throw err;
                                         }
                                         console.log("after found on db ,article saved to cache");
                                         redisRepo.setexpire(result.url);
                                     });



                                     var recoInput = {};
                                     recoInput.articleObj = result;

                                     recoInput.userObj = newuser;
                                     recoInput.refferal = profile.refferal;
                                     recoInput.label = "RD";
                                     console.log(recoInput);
                                     console.log("case:user found on cache,article found on DB: recomendation saved to redis, you get the following recomendation");
                                 }
                             });
                         }
                     });
                 } else {
                     //COmplete
                     //check here user found as well as article is found
                     //update user with the current history (updated at begining of function)
                     //call kavins function here
                     var recoInput = {};
                     recoInput.articleObj = cacheArticle;
                     recoInput.userObj = newuser;
                     recoInput.refferal = profile.refferal;
                     recoInput.label = "RD";
                     console.log("case:if user found on cache,article found on cache : recommendation are found in redis");
                 }
             }
         });
     }


     /*main case : if user not found in cache then go for articles*/
     else if (userprofile == null || undefined) {
         //if user profile is not in redis go for arr_DB
         arango_db.query("FOR user IN Users FILTER user.userID==@userid RETURN user", { 'userid': userID }, function(err, cursor) {
             if (err) {
                 console.error(err);
             } else {

                 cursor.next(function(err, result) {
                     if (result == null || undefined) {
                         // if user not found on cache make entry on cache
                         // update user with history cuurent reading article
                         var newuser = {};
                         var time = new Date();
                         newuser.userUUID = userID;
                         var newuser1 = {};
                         var history = [];

                         history.push({
                             url: profile.articleBeingRead.url,
                             time: time,
                             ref: profile.ref
                         })
                         newuser1[site] = [{ history: history }];

                         newuser[site] = JSON.stringify(newuser1[site]);



                         redisRepo.set(newuser.userUUID, newuser, function(err) {
                             if (err) {
                                 throw err;
                             }
                             console.log("new user is added to cache with updated history article saved to cache");
                             redisRepo.setexpire(newuser.userUUID);
                         });

                         //now check for articles
                         redisRepo.get(articleUrl, function(err, cacheArticle) {
                             if (err) {
                                 console.log(err);
                                 throw err;
                             } else {
                                 // if article not found on cache
                                 // find article in DB
                                 if (cacheArticle == null || undefined) {
                                     arango_db.query("FOR doc IN Document FILTER doc.url==@url RETURN doc", { 'url': articleUrl }, function(err, cursor) {
                                         if (err) {
                                             console.error(err);
                                         } else {
                                             debugger
                                             cursor.next(function(err, result) {
                                                 if (result == null || undefined) {
                                                     debugger
                                                     //Complete
                                                     // this case is executes when user is new (first time came), and aslo article is not found on redis as well as on db
                                                     // in this case we need to find realted articles by calling Kavins fun and return and save result to redis
                                                     // get the user Obj from cache
                                                     //call parameter (1.userObj,2.articleObj == null or '' ,3.referral==yes,4.label='',5.cached=false)
                                                     var recoInput = {};

                                                     debugger


                                                     redisRepo.get(newuser.userUUID, function(err, cacheUser) {
                                                         if (err) {
                                                             console.log("error");

                                                         } else {
                                                             debugger
                                                             console.log("cached User" + cacheUser);
                                                             recoInput.userObj = cacheUser;
                                                         }
                                                     })

                                                     console.log(recoInput);

                                                     setTimeout(function tout() {
                                                         console.log(recoInput);
                                                         recoInput.articleObj = '';
                                                         recoInput.refferal = profile.refferal;
                                                         //for first user request user label value is null i.e ''
                                                         recoInput.label = '';
                                                         recoInput.cached = false;



                                                         debugger
                                                         console.log("reco Input");
                                                         console.log(recoInput);
                                                         console.log("case:user not found (make entry to redis , meanse now found)and article not found , you have following recomendation")
                                                             //call here kavins function

                                                     }, 1500);
                                                 } else {
                                                     //Complete
                                                     //executes this else when new user is came , also article is not on cache but found on DB 
                                                     //make article entry to cache
                                                     var recoInput = {};
                                                     console.log(result);
                                                     redisRepo.set(result.url, result, function(err) {
                                                         if (err) {
                                                             throw err;
                                                         }
                                                         console.log("article is found on db , make entry to redis/cache");
                                                         redisRepo.setexpire(result.url);
                                                     });

                                                     ///get user from cache
                                                     redisRepo.get(newuser.userUUID, function(err, cacheUser) {
                                                         if (err) {
                                                             console.log("error");

                                                         } else {
                                                             debugger
                                                             console.log("cached User" + cacheUser);
                                                             recoInput.userObj = cacheUser;
                                                             console.log("reco Input");
                                                             console.log(recoInput);
                                                         }
                                                     })


                                                     console.log(recoInput);

                                                     setTimeout(function tout() {
                                                         console.log(recoInput);
                                                         recoInput.articleObj = result;
                                                         recoInput.refferal = profile.refferal;
                                                         //for first user request user label value is null i.e ''
                                                         recoInput.label = '';
                                                         recoInput.cached = false;

                                                         debugger

                                                         console.log("case: user not found on DB(make entry in redis, ,meanse now found)and article found on ARR_DB");
                                                         //call here kavins function
                                                         console.log(recoInput);
                                                     }, 1500);
                                                 }
                                             });
                                         }
                                     });
                                 } else {
                                     //if user not found on cache -- not on db , article found on cache    
                                     debugger
                                     var recoInput = {};
                                     recoInput.articleObj = cacheArticle;
                                     recoInput.userObj = '';
                                     recoInput.refferal = profile.refferal;
                                     recoInput.label = "RD";
                                     console.log(recoInput);
                                     console.log("case: user not found(make entry in cache), article Found on cache : you have following reco list found");
                                 }
                             }
                         });
                     } else {
                         //  if user found on ARR_DB
                         //  check for Article
                         //  get user form Arr_DB and set to cache also update user hostory




                         var userFromDb = result;


                         var UserfrmChache = JSON.parse(userFromDb[site]);

                         var newuser = userFromDb;
                         var time = new Date();
                         newuser.userUUID = userID;
                         var newuser1 = {};
                         var history = UserfrmChache[0].history;

                         history.push({
                             url: profile.articleBeingRead.url,
                             time: time,
                             ref: profile.ref
                         })
                         newuser1[site] = [{ history: history }];

                         newuser[site] = JSON.stringify(newuser1[site]);

                         UserProfile.set(userFromDb.userUUID, newuser, function(err) {
                             if (err) {
                                 throw err;
                             }
                             console.log("user from ARR_DB is saved to cache with updated current history");
                             UserProfile.setexpire(userFromDb.userUUID);
                         });



                         redisRepo.get(articleUrl, function(err, cacheArticle) {
                             if (err) {
                                 console.log(err);
                                 throw err;
                             } else {
                                 if (cacheArticle == null || undefined) {
                                     //check here if article is not found on cache
                                     //find article in DB
                                     arango_db.query("FOR doc IN Document FILTER doc.url==@url RETURN doc", { 'url': articleUrl }, function(err, cursor) {
                                         if (err) {
                                             console.error(err);
                                         } else {

                                             cursor.next(function(err, result) {
                                                 //if article not found on ARR_DB
                                                 if (result == null || undefined) {
                                                     //if user found on DB , article not found on cache
                                                     var recoInput = {};
                                                     recoInput.articleObj = '';
                                                     recoInput.userObj = userFromDb;
                                                     recoInput.refferal = profile.refferal;
                                                     recoInput.label = '';
                                                     cached = 'false'
                                                     console.log(recoInput);


                                                     console.log("case:user found on redis, article not found on DB: you get the followig recommendations");


                                                 } else {
                                                     // if article  found on ARR_DB , make entry to cache

                                                     UserProfile.set(result.url, result, function(err) {
                                                         if (err) {
                                                             throw err;
                                                         }
                                                         console.log("user found on db make article entry to redis");
                                                         UserProfile.setexpire(result.url);
                                                     });


                                                     var recoInput = {};
                                                     recoInput.articleObj = result;
                                                     recoInput.userObj = userFromDb;
                                                     recoInput.refferal = profile.refferal;
                                                     recoInput.label = '';
                                                     cached = 'false';
                                                     console.log("case:user found on redis article found on ARR_DB: you have following recomendation")
                                                 }
                                             });
                                         }
                                     })
                                 } else {
                                     //check here if article is found on cache
                                     var recoInput = {};
                                     recoInput.articleObj = cacheArticle;
                                     recoInput.userObj = userFromDb;
                                     recoInput.refferal = profile.refferal;
                                     recoInput.label = '';
                                     cached = 'false';

                                     console.log("case:user found on redis article found on redis: you have following recomendation");
                                 }
                             }
                         });
                     }
                 });
             }
         });
     }
 });
