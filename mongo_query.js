var MongoClient = require("mongodb").MongoClient;
var db;
// require("./mongo")(function(err, db) {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log("Mongodb connection established...");
//         db = db;
//     }
// });
exports.set = setUserProfile;
exports.get = getUserProfile;
//exports.getConnect = getConnect;



// function getConnect() {
//     MongoClient.connect('mongodb://localhost:27017/manotr', function(err, database) {
//         if (err) {
//             console.log(err);
//         } else {
//             db = database;
//             console.log("Mongodb Connection establisted ...");
//         }
//     })

// }


function getUserProfile(id, userdata, cb) {
    MongoClient.connect('mongodb://localhost:27017/manotr', function(err, database) {
        if (err) {
            console.log(err);
        } else {
            db = database;
            console.log("Mongodb Connection establisted ...");


            db.collection('Users').findOne({ "userId": id }, function(err, result) {
                if (err) {
                    console.log(err);
                }
                if (result === null) {

                    var newdata = {};
                    newdata.userId = id;
                    newdata.mobile = userdata.mobile;
                    newdata.region = userdata.region;
                    var dateNow = new Date();
                    newdata.dateAdded = dateNow;
                    newdata.lastupdated = dateNow;
                    newdata.history = [];
                    newdata.shows = [];
                    newdata.musics = [];
                    newdata.videos = [];
                    var historydata = {
                        type: userdata.type,
                        name: userdata.name,
                        epNo: userdata.epNo,
                        videoId: userdata.videoId,
                        songId: userdata.songId,
                        dateAdded: dateNow
                    }
                    newdata.history.push(historydata);
                    if (userdata.type == 'show') {

                        var data = {
                            name: userdata.name,
                            channel: userdata.channel,
                            last_episode_watched: userdata.videoId,
                            videos: [{
                                video: userdata.videoId
                            }]
                        }
                        newdata.shows.push(data);
                    }
                    if (userdata.type == 'music') {

                        var data = {
                            movie: userdata.name,
                            songs: [{
                                song: userdata.songId
                            }]
                        }
                        newdata.musics.push(data);
                    }
                    if (userdata.type == 'videos') {

                        newdata.videos.push({ video: userdata.videoId });
                    }
                    newdata.channels = [];
                    newdata.channels.push({ channel: userdata.channel });
                    db.collection('Users').save(newdata, function(err, result) {
                        if (err) return callback(err);
                        else {

                            //  console.log(result["ops"][0]);
                            console.log('saved in database');
                            cb(null, result["ops"][0])
                        }
                    });

                } else {
                    // var dateNow = new Date();
                    // userdata.lastupdated = dateNow;
                    // db.collection('Records').update({ "userId": id }, { $push: { "history": userdata } }, function(err, result) {
                    //     if (err) return callback(err);
                    //     else {
                    //         console.log('updated in database');
                    //     }
                    // });
                    cb(null, result);
                }
            });
        }
    })
}

function setUserProfile(id, userdata, cb) {
    MongoClient.connect('mongodb://localhost:27017/manotr', function(err, database) {
        if (err) {
            console.log(err);
        } else {
            db = database;
            console.log("Mongodb Connection establisted ...");
            console.log(userdata);
            var newdate = new Date();
            // userdata.dateAdded = newdate;
            db.collection('Users').update({ "userId": id }, { $set: userdata }, function(err, result) {
                if (err) {
                    console.log(err);
                    cb(err);
                } else {

                    //console.log('updated in database');
                    cb(null, "History updated in mongodb");
                }
            });
        }
    });
}