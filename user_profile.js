var redis = require('./redis');

exports.set = setUserProfile;
exports.get = getUserProfile;
exports.setexpire = setexpire;
exports.cleanall = cleanall;
exports.getRemainingTime = getRemainingTime;
redis.select(0);

function getRemainingTime(userId, cb) {
    redis.ttl('user:' + userId, function(err, time) {
        console.log(" Key's Remaining time:" + time);
        cb(null, time);
    });
}

function setUserProfile(userId, profile, cb) {
    redis.hmset('user:' + userId, profile, cb);
}


function setexpire(userId) {
    redis.expire('user:' + userId, 7200);
    //2hr=7200s
}


function getUserProfile(userId, cb) {
    redis.hgetall('user:' + userId, cb);
}




function cleanall() {
    redis.flushall();
}

// you use redisRepo.cleanall(); method clean redis cache